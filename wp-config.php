<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p:Q7zt5bb2`|E.k! L^j&.aEEVG+T(},m5)+!;v*DW!J#C^:cIibE$7^~(V3#Hyw' );
define( 'SECURE_AUTH_KEY',  'k }&)W0n.jC4,u%dUM[8Kmk}P6 }C$n >q>u*e9caFnHD]`;*IA?-.!)*,v{^hP.' );
define( 'LOGGED_IN_KEY',    'nGmufM;bJ1rff+[d)oY#F?Pucm,mIkjsU!8w~3-:o.vG#rHgvAR!7taZ(FAYKK+A' );
define( 'NONCE_KEY',        'C,:U13YlHG7,r,~bDk7ePhSh$ g;KsS^R?h{[3QI{Ec^g`8B7b:[I,//y]$+Z0;_' );
define( 'AUTH_SALT',        '6oj/g^Z&m6sqcUPVmtP%>XVM-Li)X9%%URVWF|#t>qv0_:`L@$$yo@<b_GdMbbRs' );
define( 'SECURE_AUTH_SALT', '`*AU%Q}46mbopw#o,JkTKM=yEI`,J//:Ch>t5U9]^Qb~k_uF*0u+3tidUt|Q~gM^' );
define( 'LOGGED_IN_SALT',   '[qN5[HY[n]|-4[QjPB%O)R)`=9Gi~*0m0!6:=3+rG^S9AS0!sx4d[VOcAv.Xyjg-' );
define( 'NONCE_SALT',       '4O|_1O00pW,7{!ce_8gr,L%vZ3]t:#/re5L`m*7Hk{`p?q!=a~VH6T&TX:`EDMYP' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
